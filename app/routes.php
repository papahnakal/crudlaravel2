<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//Route::resource('user','UserController');
//Route::resource('tabs','TabsController');

/*Route::get('/', function()
{
	return View::make('pages.home');
});*/


Route::get('about', function()
{
	return View::make('pages.about');
});
Route::get('projects', function()
{
	return View::make('pages.projects');
});
Route::get('contact', function()
{
	return View::make('pages.contact');
});
Route::get('home', function()
{
	return View::make('pages.home');
});
Route::get('/', function(){ return View::make('pages.index');});
Route::get('projects', array('as'=>'beranda','uses'=>'TabsController@projects'));

# Halaman yang berisi Form inputan Biodata baru [localhost:8000/buat]
Route::get('buat', array('as'=>'baru','uses'=>'TabsController@baru'));

# Memproses Form lalu mengirimnya kedalam database [localhost:8000/buat]
Route::post('buat', array('as'=>'buat','uses'=>'TabsController@buat'));

Route::post('buatcontent', array('as'=>'buatcontent','uses'=>'TabsController@buatcontent'));

# Menampilkan Biodata perorangan [localhost:8000/lihat/{id}]
Route::get('lihat/{id}', array('as'=>'lihat','uses'=>'TabsController@lihat'));

# Form untuk mengubah isi Biodata dalam database [localhost:8000/ubah/{id}]
Route::get('ubah/{id}', array('as'=>'ubah','uses'=>'TabsController@ubah'));

# Memproses Form lalu mengirim yang baru kedalam database [localhost:8000/ubah/{id}]
Route::put('ubah/{id}', array('as'=>'ganti','uses'=>'TabsController@ganti'));

# Tindakan untuk menghapus Biodata [localhost:8000/{id}/hapus]
Route::get('hapus/{id}', array('as' => 'hapus', 'uses' => 'TabsController@hapus'));

Route::get('login',array('as'=>'login','uses'=>'TabsController@showLogin'));
Route::post('login',array('as'=>'LakukanLogin','uses'=>'TabsController@doLogin'));


