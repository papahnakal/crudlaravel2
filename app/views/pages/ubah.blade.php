@extends('layouts.default')
@section('content')

		<!-- Kita gunaka model karena kita akan mengubah data yang telah ada -->
		{{ Form::model($biodata, array('route' => array('ganti', $biodata->id), 'method' => 'PUT')) }}
<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3>Tambah Biodata Baru</h3></div>
			<table class ="table">
			<tr>
			
			<td>{{ Form::label('nama', 'Nama') }}</td>
			<!-- Parameter kedua merupakan value, jadi terlihat terisi dengan data nama sebelumnya -->
			<td>{{ Form::text('nama', $biodata->nama,array('class'=>'form-control')) }}</td>
			<!-- Bila ada errors validasi letakkan disini 
			variabel errors ($errors) berasal dari Controller ['withErrors'] -->
			<td>@if($errors->has('nama'))
				<p class="help-block">{{ $errors->first('nama') }}</p></td>
			@endif
			</tr>
			<tr>
			<td>{{ Form::label('usia', 'Usia') }}</td>
			<td>{{ Form::text('usia', $biodata->usia,array('class'=>'form-control')) }}</td>
			<!-- Penjelasan sama seperti diatas -->
			<td>@if($errors->has('usia'))
				<p class="help-block">{{ $errors->first('usia') }}</p></td>
			@endif
			</tr>
			<tr>
			<td>{{ Form::label('jenis_kelamin', 'Jenis Kelamin') }}</td>
			<!-- Untuk select, parameter 1 = id, parameter 2 = option, parameter 3 = value -->
			<td>{{ Form::select('jenis_kelamin', array(
			'Laki-laki' => 'Laki-laki', 
			'Perempuan' => 'Perempuan',
			'lain-lain' => 'Lain-lain'), $biodata->jenis_kelamin,array('class'=>'form-control')) }}</td>
			<td>@if($errors->has('jenis_kelamin'))
				<p class="help-block">{{ $errors->first('jenis_kelamin') }}</p></td>
			@endif
			</tr>
			<tr>
			<td>{{ Form::label('telepon', 'Telepon') }}</td>
			<td>{{ Form::text('telepon', $biodata->telepon,array('class'=>'form-control')) }}</td>
			<td>@if($errors->has('telepon'))
				<p class="help-block">{{ $errors->first('telepon') }}</p><td>
			@endif
			</tr>
			<tr>
			<td>{{ Form::label('email', 'Email') }}</td>
			<td>{{ Form::text('email', $biodata->email,array('class'=>'form-control')) }}</td>
			<td>@if($errors->has('email'))
				<p class="help-block">{{ $errors->first('email') }}</p></td>
			@endif
			</tr>
			<tr>
			<td>{{ Form::label('password', 'Password') }}</td>
			<td>{{ Form::text('password','',array('class'=>'form-control')) }}</td>
			<td>@if($errors->has('password'))
				<p class="help-block">{{ $errors->first('password') }}</p></td>
			@endif
			</tr>
			<tr>
			<td></td>
			<td>{{ Form::submit('Ubah',array('class'=>'btn btn-primary btn-lg active')) }}</td>
			<td></td>
			</tr>
			</table>
			</div>
			{{ Form::close() }}

		<a href="{{ route('beranda') }}">Kembali ke index</a>
@stop