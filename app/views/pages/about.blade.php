@extends('layouts.default')
@section('content')
	
		<!-- Buka form inutan lalu ruju ke identitas route "buat" -->
	{{ Form::open(array('route' => 'buat')) }}
			<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3>Tambah Biodata Baru</h3></div>

  <!-- Table -->
  <table class="table">
			<tr>
			<td>{{ Form::label('nama', 'Nama') }}</td>
			<td>{{ Form::text('nama',Input::old('nama'),array('class'=>'form-control')) }}</td>
			<!-- Bila ada errors validasi letakkan disini 
			variabel errors ($errors) berasal dari Controller ['withErrors'] -->
			<td>@if($errors->has('nama'))
			<p class="help-block">{{ $errors->first('nama') }}</p></td>
			@endif
			</tr>
			
			<tr>
			<td>{{ Form::label('usia', 'Usia') }}</td>
			<td>{{ Form::text('usia','',array('class'=>'form-control')) }}</td>
			<!-- Penjelasan sama seperti diatas -->
			<td>@if($errors->has('usia'))
				<p class="help-block">{{ $errors->first('usia') }}</p>
			@endif
			</td>
			</tr>
			
			<tr>
			<td>{{ Form::label('jenis_kelamin', 'Jenis Kelamin') }}</td>
			<td>
			
			{{ Form::select('jenis_kelamin', array(
			'Laki-laki' => 'Laki-laki', 
			'Perempuan' => 'Perempuan',
			'lain-lain' => 'Lain-lain'),'',array('class'=>'form-control')) }}</td>
			
			
			<td>@if($errors->has('jenis_kelamin'))
				<p class="help-block">{{ $errors->first('jenis_kelamin') }}</p>
			@endif
			</td>
			</tr>
			
			<tr>
			<td>{{ Form::label('telepon', 'Telepon') }}</td>
			<td>{{ Form::text('telepon','',array('class'=>'form-control')) }}</td>
			<td>@if($errors->has('telepon'))
				<p class="help-block">{{ $errors->first('telepon') }}</p>
			@endif
			</td>
			</tr>
			
			<tr>
			<td>{{ Form::label('email', 'Email') }}</td>
			<td>{{ Form::text('email','',array('class'=>'form-control')) }}</td>
			<td>@if($errors->has('email'))
				<p class="help-block">{{ $errors->first('email') }}</p>
			@endif
			</td>
			</tr>
			
			<tr>
			<td>{{Form::label('password','Password')}}</td>
			<td>{{Form::text('password','',array('class'=>'form-control'))}}</td>
			<td>@if($errors->has('password'))
			<p class="help-block">{{$errors->first('password')}}</p>
			@endif
			</td>
			</tr>
			
			<tr>
			<td></td>
			<td>{{ Form::submit('Buat',array('class'=>'btn btn-primary btn-lg active')) }}</td>
			<td></td>
			</tr>
  </table>
</div>

		{{ Form::close() }}
@stop