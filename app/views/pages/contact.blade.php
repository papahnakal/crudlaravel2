@extends('layouts.default')
@section('content')
	@if(Session::has('pesan'))
			<p class="alert alert-info">{{ Session::get('pesan') }}</p>
		@endif
		<!-- Buka form inutan lalu ruju ke identitas route "buat" -->
	{{ Form::open(array('route' => 'buatcontent')) }}
			<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3>Buat Content</h3></div>

  <!-- Table -->
  <table class="table">
			<tr>
			<td>{{ Form::label('judul', 'Judul') }}</td>
			<td>{{ Form::text('judul',Input::old('judul'),array('class'=>'form-control')) }}</td>
			<!-- Bila ada errors validasi letakkan disini 
			variabel errors ($errors) berasal dari Controller ['withErrors'] -->
			<td>@if($errors->has('judul'))
			<p class="help-block">{{ $errors->first('judul') }}</p></td>
			@endif
			</tr>
			
			<tr>
			<td>{{ Form::label('isi', 'Isi') }}</td>
			<td>{{ Form::text('isi','',array('class'=>'form-control')) }}</td>
			<!-- Penjelasan sama seperti diatas -->
			<td>@if($errors->has('isi'))
				<p class="help-block">{{ $errors->first('isi') }}</p>
			@endif
			</td>
			</tr>
			
			<tr>
			<td>{{ Form::label('email', 'Email') }}</td>
			<td>{{ Form::text('email','',array('class'=>'form-control')) }}</td>
			<td>@if($errors->has('email'))
				<p class="help-block">{{ $errors->first('email') }}</p>
			@endif
			</td>
			</tr>
									
			<tr>
			<td></td>
			<td>{{ Form::submit('Buat',array('class'=>'btn btn-primary btn-lg active')) }}</td>
			<td></td>
			</tr>
  </table>
</div>

		{{ Form::close() }}
@stop