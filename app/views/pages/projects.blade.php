@extends('layouts.default')
@section('content')
		<!-- Siapkan variabel pesan untuk menampilkan nilai variabel yang diterima dari controller -->
		@if(Session::has('pesan'))
			<p class="alert alert-info">{{ Session::get('pesan') }}</p>
		@endif
		<!-- Jika tabel biodata memiliki isi, tampilkan isi berikut -->
		@if($biodata->count())
		<!-- Siapkan tombol untuk membuat biodata baru -->
		
		<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3>Daftar Biodata</h3></div>
  <!-- Table -->
  <center>
  <table class="table">
			<thead>
				<tr>
					<th>Nama</th>
					<th>Usia</th>
					<th>Jenis Kelamin</th>
					<th>Telepon</th>
					<th>Email</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<!-- Lakukan Perulangan untuk menampilkan seluruh isi tabel -->
				@foreach($biodata as $data)
				<tr>					
					<td>{{ $data->nama }}</td>
					<td>{{ $data->usia }}</td>
					<td>{{ $data->jenis_kelamin }}</td>
					<td>{{ $data->telepon }}</td>
					<td>{{ $data->email }}</td>
					<!-- Siapkan tombol untuk edit dan hapus item tertentu -->
					<td>
						<a href="{{ route('lihat', $data->id) }}"><span class="glyphicon glyphicon-search"></a>
						<a href="{{ route('ubah', $data->id) }}"><span class="glyphicon glyphicon-wrench"></a>
						<a href="{{ route('hapus', $data->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
					</td>
				</tr>
				@endforeach
			</tbody>
			</center>
		</table>
		</div>
		<p><a href="{{ route('baru') }}"><span class="glyphicon glyphicon-plus">Tambah</a></p>
	
		<!-- Sedangkan, bila tidak ada isinya, tampilkan isi berikut -->
		@else
		<p>Anda belum memiliki isi pada tabel biodata.</p>
		<p><a href="{{ route('baru') }}"><span class="glyphicon glyphicon-plus">Tambah</a></p>
		@endif
@stop