@extends('layouts.login')
@section('content')
	@if(Session::has('pesan'))
			<p class="alert alert-warning">{{ Session::get('pesan') }}</p>
		@endif
		<!-- Jika tabel biodata memiliki isi, tampilkan isi berikut -->
		
		<!-- Siapkan tombol untuk membuat biodata baru -->
{{ Form::open(array('route' => 'login')) }}
<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3>Login</h3></div>
		<!-- if there are login errors, show them here -->
		<table class="table">

		<tr>
			<td>{{ Form::label('email', 'Email') }}</td>
			<td>{{ Form::text('email', Input::old('email'), array('placeholder' => 'email@domain.com')) }}</td>
			<td>{{ $errors->first('email') }}</td>
		</tr>

		<tr>
			<td>{{ Form::label('password', 'Password') }}</td>
			<td>{{ Form::password('password') }}</td>
			<td>{{ $errors->first('password') }}</td>
		</tr>

		<tr><td></td><td>{{ Form::submit('Login') }}</td><td></td></tr>
		
		</table>
</div>
	{{ Form::close() }}
	
@stop