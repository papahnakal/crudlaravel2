@extends('layouts.default')
@section('content')
<div class="panel panel-primary">
 <div class="panel-heading"><h3>Informasi Biodata</h3></div>
		<center>
		<p>Nama : {{ $biodata->nama }}</p>
		<p>Usia : {{ $biodata->usia }}</p>
		<p>Jenis Kelamin : {{ $biodata->jenis_kelamin }}</p>
		<p>Telepon : {{ $biodata->telepon }}</p>
		<p>Email : {{ $biodata->email }}</p>
		<br/>
		<a href="{{ route('beranda') }}">Kembali ke Index</a>
		</center>
</div>

@stop