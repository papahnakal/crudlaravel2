<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Users List</title>

	{{ HTML::style('assets/css/bootstrap.min.css') }}
	{{ HTML::script('assets/js/jquery-1.11.1.min.js') }}
	{{ HTML::script('assets/js/bootstrap.min.js') }}

	@yield('jsblock')
</head>
<body>
<div class="container">


<div class="panel panel-default">
  <div class="panel-body"><header class="row">@include('includes.header')</header></div>
  <div class="panel-footer">
  <div id="main" class="row">

		<!-- sidebar content -->
		<div id="sidebar" class="col-md-3">@include('includes.sidebar')</div>

		<!-- main content -->
		<div id="content" class="col-md-8">@yield('content')</div>

	</div>
  </div>
</div>
	<footer class="row">@include('includes.footer')</footer>
	
</div>
</body>
</html>