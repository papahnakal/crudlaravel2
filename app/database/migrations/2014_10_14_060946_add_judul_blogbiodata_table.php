<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJudulBlogbiodataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('biodata', function(Blueprint $table)
		{
			$table->string('judul');
			$table->longtext('content');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('biodata', function(Blueprint $table)
		{
			$table->dropColumn('judul');
			$table->dropColumn('content');
		});
	}

}
