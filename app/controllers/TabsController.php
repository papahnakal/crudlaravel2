<?php
class TabsController extends BaseController {
/*public function index()
	{
		
	}
public function about()
{
		return View::make('pages.about');
}

public function contact()
{
		return View::make('pages.contact');
}*/
	public function index() {
	return View::make('pages.home'); 
	}

	# GET localhost:8000/buat
	public function baru() {
		#
		$jenis_kelamin = array(
			'Laki-laki' => 'Laki-laki', 
			'Perempuan' => 'Perempuan',
			'Lain-lain' => 'Lain-lain');
		# Tampilkan halaman pembuatan biodata
		return View::make('pages.about', compact('jenis_kelamin'));
	}

	# POST localhost:8000/buat
	public function buat() {
		#
		$input = Input::all();
		# Buat aturan validasi
		$aturan = array(
			'nama' => 'required|min:3', 
			'usia' => 'required', 
			'telepon' => 'required', 
			'email' => 'required|email',
			'password' => 'required'
		);
		# Buat pesan error validasi manual
		$pesan = array(
			'nama.required' => 'Inputan Nama wajib diisi.',
			'nama.min' => 'Inputan Nama minimal 3 karakter.',
			'usia.required' => 'Inputan Usia wajib diisi.',
			'telepon.required' => 'Inputan Telepon wajib diisi.',
			'email.required' => 'Inputan Email wajib diisi.',
			'email.email' => 'Inputan harus berupa Email.',
			'password.required'=> 'inputan harus diisi.'
		);
		# Validasi
		$validasi = Validator::make($input, $aturan, $pesan);
		# Bila validasi gagal
		if($validasi->fails()) {
			# Kembali kehalaman yang sama dengan pesan error
			return Redirect::back()->withErrors($validasi)->withInput();
		# Bila validasi sukses
		} else {
			# Buatkan variabel tiap inputan
			$nama = Input::get('nama');
			$usia = Input::get('usia');
			$jenis_kelamin = Input::get('jenis_kelamin');
			$telepon = Input::get('telepon');
			$email = Input::get('email');
			$password = Hash::make(Input::get('password'));
			# Isi kedalam database
			Biodata::create(compact('nama', 'usia', 'jenis_kelamin', 'telepon', 'email','password'));
			# Kehalaman beranda dengan pesan sukses
			return Redirect::route('beranda')->withPesan('Biodata baru berhasil ditambahkan.');
		}

	}
	public function buatcontent(){
		$input = Input::all();
		# Buat aturan validasi
		$aturan = array(
			'judul' => 'required', 
			'isi' => 'required', 
			'email' => 'required|email',		
		);
		# Buat pesan error validasi manual
		$pesan = array(
			'judul.required' => 'judul kosong!',
			'isi.required' => 'isi kosong!',
			'email.required' => 'Inputan Email wajib diisi.',
			'email.email' => 'Inputan harus berupa Email.',
			
		);
			$validasi = Validator::make($input, $aturan, $pesan);
		# Bila validasi gagal
		if($validasi->fails()) {
			# Kembali kehalaman yang sama dengan pesan error
			return Redirect::back()->withErrors($validasi)->withInput();
		# Bila validasi sukses
		} else {
			# Buatkan variabel tiap inputan
			$judul = Input::get('judul');
			$isi = Input::get('isi');
			$email = Input::get('email');
			# Isi kedalam database
			Content::create(compact('judul','isi','email'));
			# Kehalaman beranda dengan pesan sukses
			return Redirect::route('buatcontent')->withPesan('content baru berhasil ditambahkan.');
		}
	}

	# GET localhost:8000/lihat/{id}
	public function lihat($id) {
		#
		$biodata = Biodata::find($id);
		# Tampilkan view
		return View::make('pages.lihat', compact('biodata'));
	}

	# GET localhost:8000/ubah/{id}
	public function ubah($id) {
		#
		$jenis_kelamin = array(
			'Laki-laki' => 'Laki-laki', 
			'Perempuan' => 'Perempuan');
		# Tentukan biodata yang ingin diubah berdasarkan id
		$biodata = Biodata::find($id);
		# Tampilkan view
		return View::make('pages.ubah', compact('jenis_kelamin', 'biodata'));
	}

	# PUT localhost:8000/ubah/{id}
	public function ganti($id) {
		#
		$input = Input::all();
		$aturan = array(
			'nama'=>'required|min:3',
			'usia' =>'required',
			'telepon'=>'required',
			'email'=>'required|email',
			#'password'=>'required'
		);
		$pesan = array(
			'nama.required' => 'Inputan Nama wajib diisi.',
			'nama.min' => 'Inputan Nama minimal 3 karakter.',
			'usia.required' => 'Inputan Usia wajib diisi.',
			'telepon.required' => 'Inputan Telepon wajib diisi.',
			'email.required' => 'Inputan Email wajib diisi.',
			'email.email' => 'Inputan harus berupa Email.',
			#'password.required' => 'Inputan password wajib diisi.'
		);
		$validasi = Validator::make($input,$aturan,$pesan);
		if($validasi->fails()){
		return Redirect::back()->withErrors($validasi)->withInput();
		}else{
		$ganti = Biodata::find($id);
		$ganti->nama = Input::get('nama');
		$ganti->usia = Input::get('usia');
		$ganti->jenis_kelamin = Input::get('jenis_kelamin');
		$ganti->telepon = Input::get('telepon');
		$ganti->email=Input::get('email');
		$ganti->password=Hash::make(input::get('password'));
		$ganti->save();
		return Redirect::route('beranda')->withPesan('biodata berhasil ditambahkan');
		}
	}

	# DELETE localhost:8000/hapus/{id}
	public function hapus($id) {
		#
		Biodata::find($id)->delete();
		# Kembali kehalaman yang sama dengan pesan sukses
		return Redirect::back()->withPesan('Biodata berhasil dihapus.');
	}
	public function projects()
	{
		$biodata = Biodata::all();
		# Tampilkan View
		return View::make('pages.projects', compact('biodata'));
	}
	public function showLogin(){
	return View::make('pages.index');
	}
	public function doLogin(){
	$rules = array(
		'email'=>'required|email',
		'password' =>'required'
	);
	$pesan =array(
	'email.required' => 'Email kosong!.',
	'email.email' => 'Inputan harus berupa Email.',
	'password.required' => 'Password kosong!.'
	);
	$validator = Validator::make(Input::all(),$rules,$pesan);
	if($validator->fails()){
		return Redirect::back()->withErrors($validator)->withInput();
	}else{
		$userdata = array(
		'email' =>Input::get('email'),
		'password'=>Input::get('password')
		);
		if(Auth::attempt($userdata)){
		return Redirect::to('home');
		//echo('Succes!');
		}else{
		return Redirect::back()->withPesan('password atau email anda salah..');
		}
	}
	}
}